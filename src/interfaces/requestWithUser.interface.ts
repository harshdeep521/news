import { Request } from '../prize/node_modules/express';
import User from '../user/user.interface';

interface RequestWithUser extends Request {
  headers: Request.headers;
  body: Request.body;
  user: User;
  isAdmin: Boolean;
}

export default RequestWithUser;
