import { Router } from '../prize/node_modules/express';

interface Controller {
  path: string;
  router: Router;
}

export default Controller;
