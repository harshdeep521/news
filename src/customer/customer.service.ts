import Customer from './customer.model';

export const createCustomer = async (saveQueryParams) => {
    const createdData = await Customer.create(saveQueryParams);
    return createdData;
};

export const getCustomerWithPrize = async () => {
    const customerData = Customer.aggregate([{
        $lookup:
          {
            from: 'prizes',
            localField: 'code',
            foreignField: 'code',
            as: 'prize'
          }
     }]);
     return customerData;
};
