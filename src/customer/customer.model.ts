import * as mongoose from 'mongoose';

import ICustomer from './customer.interface';

const validateEmail = function (email) {
  const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  return re.test(email);
};

const customerSchema = new mongoose.Schema({
  name: { type: String, required: [true, "Please provide name"] },
  code: { type: String, required: [true, "Please provide code"] },
  email: { type: String, required: [true, "Please provide email id"], validate: [validateEmail, "Please provide valid email id"], maxlength: [200, "Email : Max 200 character"] },
  address: { type: String, required: [true, "Please provide address"] },
  size:{type:String},
  further_communucation:{type:String,required: [true, "Please Opt-in to further communications"]},
  
},
  {
    strict: false,
    versionKey: false,
    timestamps: true,
  });

const customerModel = mongoose.model<ICustomer & mongoose.Document>('Customer', customerSchema);

export default customerModel;