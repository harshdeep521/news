class ICustomer {
  _id: String;
  name: String;
  address: String;
  code: String;
  email: String;
  size:string;
  further_communucation:String
}

export default ICustomer;