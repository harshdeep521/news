import * as express from 'express';

import * as CustomerService from './customer.service';

import RequestBase from '../response/response.controller';
import { IResponse } from '../interfaces/response.interface';
import { message } from '../constants/message.constant'

class CustomerController extends RequestBase {
  public path = '/api/customer';
  public router = express.Router();

  constructor() {
    super();
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.post(`${this.path}`, this.addCustomer);

  }

  private addCustomer = async (req: express.Request, res: express.Response) => {
    try {

      await CustomerService.createCustomer(req.body);
      const resObj: IResponse = {
        res: res,
        status: 201,
        message: message.success.RES_ADD,
        data: null
      }
      this.send(resObj);
    } catch (e) {
      this.sendServerError(res, e.message);
    }
  }

}

export default CustomerController;