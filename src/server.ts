import App from './app';
import PrizeController from './prize/prize.controller';

const app = new App(
  [
    new PrizeController(),
  ],
  process.env.PORT,
);
 
app.listen();
export default app;