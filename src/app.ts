import express from 'express';
import * as bodyParser from 'body-parser';
import cors from 'cors';
import 'dotenv/config';
import https from 'https';
import fs from 'fs';
const key = fs.readFileSync(__dirname + '/certs/private.key');
const cert = fs.readFileSync(__dirname + '/certs/certificate.crt');
require('https').globalAgent.options.ca = require('ssl-root-cas').create();
import connectToTheDatabase from './dbConnection';

class App {
  public app: express.Application;
  public port: number;
  public logger;
  
  constructor(controllers, port) {
    this.app = express();
    this.port = Number(port);

    connectToTheDatabase();
    this.initializeMiddlewares();
    this.initializeControllers(controllers);
  }

  private initializeMiddlewares() {
    this.app.use(cors());
    this.app.use(express.urlencoded());
    this.app.use(express.json());
    this.app.use('/', express.static('public'));
  }

  private initializeControllers(controllers) {
    controllers.forEach((controller) => {
      this.app.use('/', controller.router);
    });
  }
  private options = {
    key: key,
    cert: cert
  };
  public listen() {
    /* const server = https.createServer(this.options, this.app);
     server.listen(this.port, () => {
     console.log(`App listening on the port ${this.port}`);
    });
    */
    this.app.listen(this.port, () => {
      console.log(`App listening on the port ${this.port}`);
    });
	
  }
}

export default App;
