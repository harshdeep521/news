export const message = {
    error: {
        RES_NAME_REQ: 'Prize name is required.',
        INVALID_CODE: 'INVALID_CODE',
        CODE_USED: 'CODE_USED'
    },

    success: {
        PRIZE_SUCCESS: 'Congratulations! You have won.',
        RES_ADD: 'Prize added successfully.',
        RES_GET: 'Prize loaded successfully.',
        RES_UPDATE: 'Prize updated successfully.',
        CUSTOMER_DETAILS: 'Your details added successfully.',
    }

}