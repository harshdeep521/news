import * as nodemailer from 'nodemailer';
import fs from 'fs';

import * as _ from 'lodash';
let sgTransport = require('nodemailer-sendgrid-transport');
export class Notification {

    preSendEmail = (template, templateData = {}) => {
        const finalTemp = this.getHtmlTemplate(template, templateData);
        console.log(this.sendEmail(finalTemp, templateData)+"i am here");
    };
    getHtmlTemplate = (templatePath, templateData) => {
        try {
            const html = fs.readFileSync(templatePath, 'utf8');
            const compiled = _.template(html);
            return compiled(templateData);
        } catch (error) {
            console.error(error);
        }
    };

    public sendEmail(template, templateData) {
        return new Promise((resolve, reject) => {
            var options = {
                auth: {
                    api_key: process.env.SENDGRID_API_KEY
                },
                secureConnection: false,
            }
            const transporter = nodemailer.createTransport(sgTransport(options));
            const mailOptions = {
                to: templateData.to,
                from: process.env.EMAIL_USER_ID,
                subject: templateData.subject,
                html: template,
            };

            transporter.sendMail(mailOptions, function (error, info) {
                if (error) {
                    console.log('error', error);
                    reject(error);
                } else {
                    console.log('info', info);
                    resolve(info);
                }
            });
        });
    }

}
export default new Notification;