import * as mongoose from 'mongoose';

import IPrize from './prize.interface';

const prizeSchema = new mongoose.Schema({
  name: { type: String, required: true },
  code: { type: String, required: true, unique: true },
  message: { type: String, required: false },
  count: { type: Number, default: 1 },
  remainingCount: { type: Number, default: 1 },
},
  {
    strict: false,
    versionKey: false,
    timestamps: true,
  });

const prizeModel = mongoose.model<IPrize & mongoose.Document>('Prize', prizeSchema);

export default prizeModel;