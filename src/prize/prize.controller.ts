import * as express from 'express';
import * as csv from 'fast-csv';
import fs from 'fs';
import path from 'path';

import * as PrizeService from './prize.service';
import * as CustomerService from '../customer/customer.service';

import RequestBase from '../response/response.controller';
import { IResponse } from '../interfaces/response.interface';
import { message } from '../constants/message.constant';
import notification from '../utils/notification';
import { capitalize } from '../utils/common';

class PrizeController extends RequestBase {
  public path = '/api/prize';
  public router = express.Router();

  constructor() {
    super();
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.post(`${this.path}`, this.addPrize);
    this.router.get(`${this.path}/customer`, this.getPrizeWithCustomer);
    this.router.get(`${this.path}/all`, this.getPrizes);
    this.router.get(`${this.path}`, this.getPrize);
    this.router.patch(`${this.path}`, this.updatePrizeCount);
    this.router.post(`/api/customer`, this.addCustomer);
  }

  /**
   * Copy prize.csv file to the root folder and call service to seed the db from CSV
   * @param req 
   * @param res 
   */
  private addPrize = async (req: express.Request, res: express.Response) => {
    try {
      let prizes = [];
      fs.createReadStream('./prize.csv')
        .pipe(csv.parse({ headers: true }))
        .on('data', async row => {
          let obj = {
            name: row['Prize Name'].trim(),
            code: row['Code'],
            message: row['Message']
          }

          prizes.push(obj)
        }).on('end', async () => {
          const uniqPrizes = [];
          prizes.map((p) => {
            let index = uniqPrizes.findIndex((i) => i.code === p.code);
            // console.log('index', index)
            if (index === -1) {
              uniqPrizes.push({
                ...p,
                count: 1,
                remainingCount: 1
              })
            } else {
              uniqPrizes[index].count += 1;
              uniqPrizes[index].remainingCount += 1;
            }
          })

          /**Insert data to database */
          await PrizeService.createPrize(uniqPrizes);
        }).on('error', async (err) => {
          console.log('error', err);
          this.sendServerError(res, 'Error while reading the CSV');
        })
      const resObj: IResponse = {
        res: res,
        status: 201,
        message: message.success.RES_ADD,
        data: null
      }
      this.send(resObj);
    } catch (e) {
      this.sendServerError(res, e.message);
    }
  }

  /**
   * Get all available customers in database with prize name (CSV)
   * @param req 
   * @param res 
   */
  private getPrizeWithCustomer = async (req: express.Request, res: express.Response) => {
    const data = await CustomerService.getCustomerWithPrize();
    const customerPrize = data.map((d: any) => {
      let a: any = {
        ...d,
        time: `"${d.createdAt.toLocaleString("en-US", { timeZone: "Pacific/Auckland" })}"`,
        prize: d.prize && d.prize.length && d.prize[0].name ? d.prize[0].name : '',
        address: d.address ? `"${d.address}"` : ''
      };
      return a;
    });
    const resObj: IResponse = {
      res: res,
      status: 200,
      message: message.success.RES_GET,
      data: customerPrize
    }
    this.send(resObj);

  }

  /**
   * Get prize details from code
   * @param req 
   * @param res 
   */
  private getPrize = async (req: express.Request, res: express.Response) => {
    try {
      const prize: any = await PrizeService.getPrize(req.query.code);
      if (!prize) {
        return this.sendBadRequest(res, message.error.INVALID_CODE);
      }
      if (prize.remainingCount) {
        const resObj: IResponse = {
          res: res,
          status: 200,
          message: message.success.RES_GET,
          data: prize
        }
        this.send(resObj);
      } else {
        return this.sendBadRequest(res, message.error.CODE_USED);
      }

    } catch (e) {
      return this.sendServerError(res, e.message);
    }
  }

  /**
   * Get all available prizes in database
   * @param req 
   * @param res 
   */
  private getPrizes = async (req: express.Request, res: express.Response) => {
    try {
      const prizes: any = await PrizeService.getPrizes();
      const resObj: IResponse = {
        res: res,
        status: 200,
        message: message.success.RES_GET,
        data: prizes
      }
      this.send(resObj);

    } catch (e) {
      return this.sendServerError(res, e.message);
    }
  }

  getImageURL = (prizeName) => {
    switch (prizeName) {
      case 'PTR Chilly Bin':
        return `${process.env.imageBaseURL}/Prizes_ChillyBin.png`;
      case 'PTR x Checks Downtown Jacket':
        return `${process.env.imageBaseURL}/Prizes_Jacket.png`;
      case 'PTR Tote Bag':
        return `${process.env.imageBaseURL}/Prizes_Tote.png`;
      case 'PTR Beanie':
        return `${process.env.imageBaseURL}/Prizes_beanie.png`;
      default:
        return `${process.env.imageBaseURL}/PTR_KiwisForKiwis_Background_01.png`;
    }
  }
  /**
   * Reduce remaining prize count
   * @param req 
   * @param res 
   */
  private updatePrizeCount = async (req: express.Request, res: express.Response) => {
    console.log(req.body);
    try {
      const prize: any = await PrizeService.getPrize(req.body.code);
      if (!prize) {
        return this.sendBadRequest(res, message.error.INVALID_CODE);
      }

      if (prize.remainingCount <= 0) {
        return this.sendBadRequest(res, message.error.CODE_USED);
      }

      // const updatedPrize: any = await PrizeService.updatePrizeCount(req.body.code);

      const resObj: IResponse = {
        res: res,
        status: 200,
        message: message.success.PRIZE_SUCCESS,
        data: {
          image: this.getImageURL(prize.name),
          ...prize._doc
        }
      }
      this.send(resObj);
    } catch (e) {
      this.sendServerError(res, e.message);
    }
  }

  /**
   * Add customer with prize code
   * @param req 
   * @param res 
   */
  private addCustomer = async (req: express.Request, res: express.Response) => {
    try {
      const updatedPrize: any = await PrizeService.getPrize(req.body.code);
      if (!updatedPrize) {
        return this.sendBadRequest(res, message.error.INVALID_CODE);
      }

      if (updatedPrize.remainingCount <= 0) {
        return this.sendBadRequest(res, message.error.CODE_USED);
      }
     
      const customer: any = await CustomerService.createCustomer(req.body);
      await PrizeService.updatePrizeCount(req.body.code);

      const emailDetails = req.body;
      console.log('i have reach here');
      /**
       * Email to customer
       */
      if(typeof emailDetails.invalid!="undefined"){
      const customerTemplatePath = path.join(__dirname, '../templates/customer.template.html');
      emailDetails.to = emailDetails.email
      emailDetails.subject = 'Congratulations! You Won a Prize'
      emailDetails.prize = updatedPrize.name
      emailDetails.fullName = capitalize(emailDetails.name)
      notification.preSendEmail(customerTemplatePath, emailDetails)

      const adminTemplatePath = path.join(__dirname, '../templates/admin.template.html');
      emailDetails.to = "harshdeep521@gmail.com"
      emailDetails.subject = 'Customer Won a Prize!'
      emailDetails.prize = updatedPrize.name
      emailDetails.fullName = capitalize(emailDetails.name)
     
      notification.preSendEmail(adminTemplatePath, emailDetails)

      }
       
      /**
       * Email to admin
       */
      const adminTemplatePath = path.join(__dirname, '../templates/admin.template.html');
      emailDetails.to = process.env.EMAIL_ADMIN
      emailDetails.subject = 'Customer Won a Prize!'
      emailDetails.prize = updatedPrize.name
      emailDetails.fullName = capitalize(emailDetails.name)
     
      notification.preSendEmail(adminTemplatePath, emailDetails)

      const resObj: IResponse = {
        res: res,
        status: 200,
        message: message.success.CUSTOMER_DETAILS,
        data: customer
      }
      this.send(resObj);

    } catch (e) {
      return this.sendServerError(res, e.message);
    }
  }

}

export default PrizeController;