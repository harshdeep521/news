class IPrize {
  _id: String;
  name: String;
  code: String;
  message: String;
  count: Number;
}

export default IPrize;