import prizeModel from './prize.model';

export const createPrize = async (saveQueryParams) => {
    const createdData = await prizeModel.insertMany(saveQueryParams);
    return createdData;
};

export const getPrize = async (code) => {
    const createdData = await prizeModel.findOne({ code });
    return createdData;
};

export const getPrizes = async (skip = 0, limit = 0) => {
    const prizes = await prizeModel.find().skip(skip).limit(limit);
    return prizes;
};

export const prizeCount = async () => {
    const count = await prizeModel.find().countDocuments();
    return count;
}

export const updatePrizeCount = async (code) => {
    const createdData = await prizeModel.findOneAndUpdate({ code },
        { $inc: { remainingCount: -1 } }, { new: true });
    return createdData;
};
